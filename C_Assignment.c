#include <stdio.h>
#include <math.h>

int main() {
    double a, b, c, discriminant, root1, root2, realPart, imaginaryPart;

    printf("Enter coefficient a: ");
    scanf("%lf", &a);
    printf("Enter coefficient b: ");
    scanf("%lf", &b);
    printf("Enter coefficient c: ");
    scanf("%lf", &c);

    if (a == 0) {
        printf("Error: a cannot be zero\n");
    } else {
        discriminant = b * b - 4 * a * c;
        if (discriminant > 0) {
            printf("roots are real and Distinct \n");
            root1 = (-b + sqrt(discriminant)) / (2 * a);
            root2 = (-b - sqrt(discriminant)) / (2 * a);
            printf("Root1 = %lf\n", root1);
            printf("Root2 = %lf\n", root2);
        } else if (discriminant == 0) {
            printf("roots are real and equal\n");
            double root = -b / (2 * a);
            printf("Root = %lf\n", root);
        } else {
            printf("Complex roots\n");
            realPart = -b / (2 * a);
            imaginaryPart = sqrt(-discriminant) / (2 * a);
            printf("Root1 = %lf + %lfi\n", realPart, imaginaryPart);
            printf("Root2 = %lf - %lfi\n", realPart, imaginaryPart);
        }
    }

    
    return 0;
}
